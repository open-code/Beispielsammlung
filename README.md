# Beispielsammlung
Dieses Repository trägt zu verschiedenen Themen eine wachsende Sammlung an Beispielen zum Umgang und Lösungen aus der openCode Community bei.

[[_TOC_]]

## Lizenzierung des Projektes
Die Open-Source-Lizenzierung eines Projekts ist wichtig, um Nachnutzenden den Einsatz und die Anpassung des Projekts rechtssicher zu ermöglichen. Eine Open-Source-Lizenzierung ist grundsätzlich Vorraussetzung für öffentliche Projekte auf Open CoDE.
- Auswahl einer durch openCode freigegebenen Open-Source-Lizenz: [openCode Allowliste](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/Lizenzcompliance)

```
ToDo: Skalierung des Gifs, dann hier einfügen
![](/docs/images/lizenz_hinzufügen.GIF)
```
- Lizenzierung nach EUPL-1.2 mit LICENSE Datei und README Vermerk: [Projekt _KoliBri_ Lizenz](https://gitlab.opencode.de/OC000005112572/kolibri/-/blob/main/LICENSE), [Projekt _KoliBri_ README](https://gitlab.opencode.de/OC000005112572/kolibri/-/blob/main/README.md#lizenz)
- Lizenzierung nach EUPL-1.2 mit LICENSE Datei und README Vermerk:
[Projekt _EGVP-Address Book_ Lizenz](https://gitlab.opencode.de/ovgrlp/Egvp-AddressBook/-/blob/primary/LICENSE), [Projekt _EGVP-AddressBook_ README](https://gitlab.opencode.de/ovgrlp/Egvp-AddressBook/-/blob/primary/README.md#lizenz)
- Lizenzierung nach AGPL-3.0-or-later mit LICENSE Datei: [Projekt _LibretranslateGUI_ Lizenz](https://gitlab.opencode.de/regional.digital/LibretranslateGUI/-/blob/main/LICENSE)
- Lizenzierung nach Apache-2.0 mit LICENSE Datei: [Projekt _Konformitaetstest_ Lizenz](https://gitlab.opencode.de/ig-bvc/konformitaetstest/-/blob/main/LICENSE)

## Software Bill of Materials (SBOM)
Die kommunikation von Sofwater Bill of Materials (SBOM) ist eine Anforderung der Nutzungsbedingungen, wenn Kompilate, Binaries oder Ähnliches wie Containter Images auf openCode verbreitet werden. Hintergrund ist, dass in einem Kompilat, Binary oder vergleichbaren Artefakten nicht ersichtlich ist ,welche Komponenten mitgeliefert werden.

- SPDX Dokument eines Java Projekts: [Projekt _SPDX Conformance Referenzprojekt_ SBOM](https://gitlab.opencode.de/open-code/spdx-conformance/-/blob/main/Referenzprojekt1/spdx/SPDX_opencode-test-1-0-SNAPSHOT-jar.yml)
- SPDX Dokument eines Frontends mit Vue.js: [Projekt _Collaboratives Online Board - Frontend_ SBOM](https://gitlab.opencode.de/iqsh/collaboration-online-board/frontend/-/blob/main/SPDX_v1.0.0.yml)

## Security Praktiken
Die Dokumentation von Security Praktiken und ggf. deren Ergebnissen trägt zur Transparenz bei und ermöglicht Nachnutzenden einen ersten Ansatzpunkt in der Einsatzentscheidung.

- Hinweis und Kontaktadresse zur nicht-öffentlichen Meldung von Sicherheitslücken: [Projekt _KoliBri_ SECURITY.md](https://gitlab.opencode.de/OC000005112572/kolibri/-/blob/main/docs/SECURITY.md)
- Kommunikation von Security Scan Ergebnissen für CVE Funde: [Projekt _Nextcloud_ Scan Ergebnisse](https://gitlab.opencode.de/ig-bvc/demo-apps/nextcloud#security-scan-ergebnisse)


## Synchronisierung aus anderen Repositories
Das Spiegeln von Repository Inhalten von anderen Code Versionierungssystemen nach openCode kann durch Vielfältige Mechanismen erfolgen, wenn im anderen Code Versionierungssystemen aktiv weiterentwickelt werden soll. Dieser Fall ist von einer Portierung und aktiven Weiterentwicklung auf openCode abzutrennen.

- Ein-Wege Synchronisation aus GitHub nach openCode via GitHub Actions und festem Schedule: [Projekt _KoliBri_](https://gitlab.opencode.de/OC000005112572/kolibri/-/blob/main/.github/workflows/sync-to-opencode.yml)
- Ein-Wege Synchronisation aus GitHub nach openCode via openCode GitLab CI/CD und optionalem Schedule: [Projekt _Repository-Mirroring-Practices_](https://gitlab.opencode.de/CoDE_Admin/repository-mirroring-ci-cd-example/-/blob/main/.gitlab-ci.yml)

## Dokumentation eines Projekts
Die umfangreiche Dokumentation eines Projekts in der README ist keine Grundvorraussetzung für Projekte auf openCode, jedoch erleichtert ein gut dokumentiertes Projekt allen Nachnutzenden den Einstieg zum Einsatz oder der Weiterentwicklung des Projekts.

- Schritt für Schritt Anleitung bis zur Einbindung des Projekts als Komponente: [Projekt _KoliBri_ Get Started](https://gitlab.opencode.de/OC000005112572/kolibri/-/blob/main/docs/GET_STARTED.md)
- Gebrauchsanleitung für Endnutzer in verschiedenen Konfigurationen: [Projekt _Konformitaetstest_ Gebrauchsanleitung](https://gitlab.opencode.de/ig-bvc/konformitaetstest/-/blob/main/README.md#gebrauchsanleitung-f%C3%BCr-endnutzer)
- Quick-Start Einrichtung für Endnutzer und umfangreiche Dokumentation für Entwickelnde: [Projekt _Masterportal_ Setup](https://gitlab.opencode.de/geowerkstatt-hamburg/masterportal/-/blob/dev/doc/setup.md), [Projekt _Masterportal_ Entwickler Dokumentation](https://gitlab.opencode.de/geowerkstatt-hamburg/masterportal/-/blob/dev/doc/devdoc.md)
- Kurze Installationsdokumentation mit Build-Hinweisen: [Projekt _Ressourcenplanung_ Intallation](https://gitlab.opencode.de/ovgrlp/ressourcenplanung#installation)

## Steuerung von Contributions
Um Interessierten und Nachnutzenden den Weg zu vereinfachen an der Weiterentwicklung eines Projekts zu partizipieren empfiehlt es sich die Möglichkeiten und präferierten Kontaktwege für Contributions klar darzulegen.

- CONTRIBUTING Datei mit Anleitung zu Bug Report und Pull Request: [Projekt _Validierungsregeln_ CONTRIBUTING.md](https://gitlab.opencode.de/xleitstelle/xplanung/validierungsregeln/standard/-/blob/master/CONTRIBUTING.md)
- CONTRIBUTING Datei mit Anleitung zu Bug Report, Pull Request, FAQ, eigenem Code of Conduct und Kontaktadresse: [Projekt _KoliBri_ CONTRIBUTING.md](https://gitlab.opencode.de/OC000005112572/kolibri/-/blob/main/CONTRIBUTING.md)
- Information über aktuelle Hindernisse der Beteiligung: [Projekt _Bundesmessenger_](https://gitlab.opencode.de/bwi/bundesmessenger/info#mitarbeit)
- CONTRIBUTING Datei mit Anleitung zu Bug Report und Pull Request:  [Projekt _OZGxPlanung_ CONTRIBUTING.md](https://gitlab.opencode.de/diplanung/ozgxplanung/-/blob/main-v5.0.3/CONTRIBUTING.md)
